﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TestForms.DataAccess.Interfaces
{
    public interface IRepository<T0,T1,T2>
    {
        T0 DbContext { get; set; }
        IEnumerable<T1> GetAll();
        IEnumerable<T1> GetByCriteria(T2 criteria);
        IEnumerable<T1> GetByValue(string value);
        T1 Get(int id);
        Task Create(T1 item);
        Task Update(T1 item);
        void Delete(T1 item);
    }
}