﻿using System.Collections.Generic;
using TestForms.DataAccess.Contexts;
using TestForms.Entity;

namespace TestForms.DataAccess.Repositories
{
    public class ProfessionRepository : BaseRepository<Profession, object>
    {
        public ProfessionRepository(TestFormsDbContext context) : base(context)
        {

        }

        public override IEnumerable<Profession> GetAll()
        {
            return DbContext.Professions;
        }

        public override IEnumerable<Profession> GetByCriteria(object criteria)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerable<Profession> GetByValue(string value)
        {
            throw new System.NotImplementedException();
        }

        public override Profession Get(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}