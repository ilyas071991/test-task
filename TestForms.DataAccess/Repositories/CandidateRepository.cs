﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestForms.DataAccess.Contexts;
using TestForms.Entity;

namespace TestForms.DataAccess.Repositories
{
    public class CandidateRepository : BaseRepository<Candidate, CandidateSearchCriteria>
    {
        public CandidateRepository(TestFormsDbContext context) : base(context)
        {

        }

        public override IEnumerable<Candidate> GetAll()
        {
            return DbContext.Candidates
                .Include(c => c.Gender)
                .Include(c => c.Profession);
        }

        public override IEnumerable<Candidate> GetByCriteria(CandidateSearchCriteria criteria)
        {
            IQueryable<Candidate> candidates = DbContext.Candidates;

            if (criteria == null) return candidates.ToList();

            if (!string.IsNullOrEmpty(criteria.Name))
                candidates = candidates.Where(c => EF.Functions.Like(c.Name, criteria.Name));
            if (!string.IsNullOrEmpty(criteria.LastName))
                candidates = candidates.Where(c => EF.Functions.Like(c.LastName, criteria.LastName));
            if (criteria.Profession != null)
                candidates = candidates.Where(c => c.Profession == criteria.Profession);
            if (criteria.YearsOfExperience != null)
                candidates = candidates.Where(c => c.YearsOfExperience >= criteria.YearsOfExperience);

            candidates.Include(c => c.Gender)
                .Include(c => c.Profession);

            return candidates.ToList();
        }

        public override IEnumerable<Candidate> GetByValue(string value)
        {
            IQueryable<Candidate> entities = DbContext.Candidates
                .Include(c => c.Gender)
                .Include(c => c.Profession);

            if (string.IsNullOrEmpty(value)) return entities.ToList();

            var type = typeof(Candidate);

            var properties = typeof(Candidate).GetProperties();

            var filteredCandidates = new List<Candidate>();

            foreach (var entity in entities)
            {
                var propertiesValue = properties.Where(prop => !prop.PropertyType.IsClass || prop.PropertyType == typeof(String))
                        .Select(prop => type.GetProperty(prop.Name)?.GetValue(entity, null));

                var filteredPropValues = propertiesValue
                    .Any(propValue => propValue != null! && propValue.ToString().Contains(value, StringComparison.OrdinalIgnoreCase));

                if (filteredPropValues)
                    filteredCandidates.Add(entity);
            }

            return filteredCandidates.ToList();
        }

        public override Candidate Get(int id)
        {
            return DbContext.Candidates
                .Include(c => c.Gender)
                .Include(c => c.Profession)
                .FirstOrDefault(c => c.Id == id);
        }

        public override async Task Create(Candidate candidate)
        {
            var existedGender =
                DbContext.Genders.FirstOrDefault(p => p.Title.Equals(candidate.Gender.Title));

            if (existedGender != null)
            {
                candidate.Gender = existedGender;
                DbContext.Entry(candidate.Gender).State = EntityState.Unchanged;
            }

            var existedProfession =
                DbContext.Professions.FirstOrDefault(e => e.Title.Equals(candidate.Profession.Title));
            if (existedProfession != null)
            {
                candidate.Profession = existedProfession;
                DbContext.Entry(candidate.Profession).State = EntityState.Unchanged;
            }

            DbContext.Add(candidate);

            await DbContext.SaveChangesAsync();
        }

        public override async Task Update(Candidate candidate)
        {
            await base.Update(candidate);
        }

        public override void Delete(Candidate candidate)
        {
            base.Delete(candidate);
        }
    }
}