﻿using System.Collections.Generic;
using TestForms.DataAccess.Contexts;
using TestForms.Entity;

namespace TestForms.DataAccess.Repositories
{
    public class GenderRepository : BaseRepository<Gender, object>
    {
        public GenderRepository(TestFormsDbContext context) : base(context)
        {

        }

        public override IEnumerable<Gender> GetAll()
        {
            return DbContext.Genders;
        }

        public override IEnumerable<Gender> GetByCriteria(object criteria)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerable<Gender> GetByValue(string value)
        {
            throw new System.NotImplementedException();
        }

        public override Gender Get(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}