﻿using System;
using TestForms.DataAccess.Contexts;
using TestForms.DataAccess.Repositories;

namespace TestForms.DataAccess
{
    public class UnitOfWork : IDisposable
    {
        private readonly TestFormsDbContext _dbContext;
        private CandidateRepository _candidateRepository;
        private ProfessionRepository _professionRepository;
        private GenderRepository _genderRepository;
        private bool _disposed;

        public UnitOfWork(TestFormsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public CandidateRepository Candidates => _candidateRepository ??= new CandidateRepository(_dbContext);
        public ProfessionRepository Professions => _professionRepository ??= new ProfessionRepository(_dbContext);
        public GenderRepository Genders => _genderRepository ??= new GenderRepository(_dbContext);

        public void Save()
        {
            _dbContext.SaveChangesAsync();
        }

        public void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}