﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestForms.DataAccess.Contexts;
using TestForms.DataAccess.Interfaces;

namespace TestForms.DataAccess
{
    public abstract class BaseRepository<TItemModel, TSearchCriteria> : IRepository<TestFormsDbContext, TItemModel, TSearchCriteria> where TItemModel : class
    {
        protected BaseRepository(TestFormsDbContext dbContext)
        {
            DbContext = dbContext;
            DbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public TestFormsDbContext DbContext { get; set; }

        public DbSet<TItemModel> DbSet { get; set; }

        public abstract IEnumerable<TItemModel> GetAll();

        public abstract IEnumerable<TItemModel> GetByCriteria(TSearchCriteria criteria);

        public abstract IEnumerable<TItemModel> GetByValue(string value);

        public abstract TItemModel Get(int id);

        public virtual async Task Create(TItemModel item)
        {
            await DbContext.AddAsync(item);
            await DbContext.SaveChangesAsync();
        }

        public virtual async Task Update(TItemModel item)
        {
            try
            {
                DbContext.Update(item);
                await DbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                var entity = ex.Entries.Single().GetDatabaseValues();

                if (entity == null)
                {
                    //TODO: To think how to correctly handle DbUpdateConcurrencyException in Update method if entity has been deleted before
                }
                else
                {
                    ex.Entries.Single().Reload();
                    await DbContext.SaveChangesAsync();
                }
            }
        }

        public virtual void Delete(TItemModel item)
        {
            var deleted = false;
            while (!deleted)
            {
                try
                {
                    DbContext.Remove(item);
                    DbContext.SaveChanges();
                    deleted = true;
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    //TODO: To think how to correctly handle DbUpdateConcurrencyException in Delete method
                    var entry = ex.Entries.Single();
                    if (entry.State == EntityState.Deleted)
                    {
                        entry.State = EntityState.Detached;
                    }
                    else
                    {
                        entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                    }
                }
            }
        }
    }
}