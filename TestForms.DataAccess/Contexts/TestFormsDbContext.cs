﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TestForms.Entity;
using TestForms.Entity.Enums;

namespace TestForms.DataAccess.Contexts
{
    public sealed class TestFormsDbContext : DbContext
    {
        public DbSet<Candidate> Candidates { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<Profession> Professions { get; set; }

        public TestFormsDbContext(DbContextOptions<TestFormsDbContext> options)
            : base(options)
        {
            if (Database.EnsureCreated())
            {
                DbInit();
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=testformsdb;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var candidateProperties = typeof(Candidate).GetProperties().Where(p => !p.PropertyType.IsClass || p.PropertyType == typeof(String));

            foreach (var propertyInfo in candidateProperties)
            {
                modelBuilder.Entity<Candidate>().HasIndex(propertyInfo.Name).HasFilter($"[{propertyInfo.Name}] IS NOT NULL");
            }
        }

        private void DbInit()
        {
            var gender1 = new Gender { Title = GenderType.Male.ToString() };
            var gender2 = new Gender { Title = GenderType.Female.ToString() };

            var profession1 = new Profession { Title = ProfessionType.Analyst.ToString() };
            var profession2 = new Profession { Title = ProfessionType.Developer.ToString() };
            var profession3 = new Profession { Title = ProfessionType.Manager.ToString() };
            var profession4 = new Profession { Title = ProfessionType.Tester.ToString() };


            Genders.AddRange(gender1, gender2);
            Professions.AddRange(profession1, profession2, profession3, profession4);

            var candidate1 = new Candidate
            {
                Age = 29,
                DateOfBirth = new DateTime(1991, 01, 01),
                YearsOfExperience = 5,
                Gender = gender1,
                LastName = "Ivanov",
                MiddleName = string.Empty,
                Name = "Ivan",
                Profession = profession1
            };

            var candidate2 = new Candidate
            {
                Age = 30,
                DateOfBirth = new DateTime(1990, 10, 10),
                YearsOfExperience = 7,
                Gender = gender1,
                LastName = "Petrov",
                MiddleName = string.Empty,
                Name = "Petr",
                Profession = profession2
            };

            var candidate3 = new Candidate
            {
                Age = 22,
                DateOfBirth = new DateTime(1998, 08, 10),
                YearsOfExperience = 1,
                Gender = gender2,
                LastName = "Ivanova",
                MiddleName = string.Empty,
                Name = "Elena",
                Profession = profession3
            };

            var candidate4 = new Candidate
            {
                Age = 25,
                DateOfBirth = new DateTime(1995, 04, 15),
                YearsOfExperience = 3,
                Gender = gender2,
                LastName = "Pavlova",
                MiddleName = "Pavlovna",
                Name = "Elizaveta",
                Profession = profession4
            };

            Candidates.AddRange(candidate1, candidate2, candidate3, candidate4);

            SaveChanges();
        }
    }
}
