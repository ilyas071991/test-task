﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestForms.BusinessLogic.Interfaces;
using TestForms.DataAccess;
using TestForms.DataAccess.Contexts;
using TestForms.Entity;

namespace TestForms.BusinessLogic
{
    public class CandidateBusinessLogic: IBusinessLogic<Candidate, CandidateSearchCriteria>
    {
        private readonly UnitOfWork _unitOfWork;

        public CandidateBusinessLogic(TestFormsDbContext context)
        {
            _unitOfWork = new UnitOfWork(context);
        }

        public IEnumerable<Candidate> GetAll()
        {
            return _unitOfWork.Candidates.GetAll().ToList();
        }

        public IEnumerable<Candidate> GetByCriteria(CandidateSearchCriteria criteria)
        {
            return _unitOfWork.Candidates.GetByCriteria(criteria).ToList();
        }

        public IEnumerable<Candidate> GetByValue(string value)
        {
            return _unitOfWork.Candidates.GetByValue(value).ToList();
        }

        public Candidate Get(int candidateId)
        {
            return _unitOfWork.Candidates.Get(candidateId);
        }

        public async Task Create(Candidate candidate)
        {
            await _unitOfWork.Candidates.Create(candidate);
        }

        public async Task Update(Candidate candidate)
        {
            await _unitOfWork.Candidates.Update(candidate);
        }

        public void Delete(Candidate candidate)
        {
            _unitOfWork.Candidates.Delete(candidate);
        }
    }
}