﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestForms.BusinessLogic.Interfaces;
using TestForms.DataAccess;
using TestForms.DataAccess.Contexts;
using TestForms.Entity;

namespace TestForms.BusinessLogic
{
    public class GenderBusinessLogic : IBusinessLogic<Gender, object>
    {
        private readonly UnitOfWork _unitOfWork;

        public GenderBusinessLogic(TestFormsDbContext context)
        {
            _unitOfWork = new UnitOfWork(context);
        }

        public IEnumerable<Gender> GetAll()
        {
            return _unitOfWork.Genders.GetAll().ToList();
        }

        public IEnumerable<Gender> GetByCriteria(object criteria)
        {
            throw new System.NotImplementedException();
        }

        public Gender Get(int candidateId)
        {
            throw new System.NotImplementedException();
        }

        public Task Create(Gender item)
        {
            throw new System.NotImplementedException();
        }

        public Task Update(Gender item)
        {
            throw new System.NotImplementedException();
        }

        public void Delete(Gender item)
        {
            throw new System.NotImplementedException();
        }
    }
}