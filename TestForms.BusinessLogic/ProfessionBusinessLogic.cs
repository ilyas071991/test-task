﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestForms.BusinessLogic.Interfaces;
using TestForms.DataAccess;
using TestForms.DataAccess.Contexts;
using TestForms.Entity;

namespace TestForms.BusinessLogic
{
    public class ProfessionBusinessLogic : IBusinessLogic<Profession, object>
    {
        private readonly UnitOfWork _unitOfWork;

        public ProfessionBusinessLogic(TestFormsDbContext context)
        {
            _unitOfWork = new UnitOfWork(context);
        }

        public IEnumerable<Profession> GetAll()
        {
            return _unitOfWork.Professions.GetAll().ToList();
        }

        public IEnumerable<Profession> GetByCriteria(object criteria)
        {
            throw new System.NotImplementedException();
        }

        public Profession Get(int candidateId)
        {
            throw new System.NotImplementedException();
        }

        public Task Create(Profession item)
        {
            throw new System.NotImplementedException();
        }

        public Task Update(Profession item)
        {
            throw new System.NotImplementedException();
        }

        public void Delete(Profession item)
        {
            throw new System.NotImplementedException();
        }
    }
}