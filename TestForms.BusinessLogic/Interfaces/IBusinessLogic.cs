﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TestForms.BusinessLogic.Interfaces
{
    public interface IBusinessLogic<T1,T2>
    {
        IEnumerable<T1> GetAll();
        IEnumerable<T1> GetByCriteria(T2 criteria);
        T1 Get(int id);
        Task Create(T1 item);
        Task Update(T1 item);
        void Delete(T1 item);
    }
}