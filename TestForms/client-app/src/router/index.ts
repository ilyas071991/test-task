import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
    {
        path: '/',
        name: 'Search',
        component: () => import(/* webpackChunkName: "search" */ '../views/Search/Search.vue')
    },
    {
        path: "/candidates/:id",
        name: "Candidate",
        component: () => import("../components/Candidate/Candidate.vue")
    },
    {
        path: "/candidates/new",
        name: "CandidateNew",
        component: () => import("../components/Candidate/Candidate.vue")
    }
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

export default router;
