import { Component, Vue, Prop, Watch } from 'vue-property-decorator';
import { CandidateModel } from '../../entities/CandidateModel';
import { Gender } from '../../entities/Gender';
import { Profession } from '../../entities/Profession';
import DataService from "../../services/DataService";


@Component({ components: {} })
export default class Candidate extends Vue {

    private url = "candidates";
    form: CandidateModel = new CandidateModel();

    nameDirty = false;
    lastNameDirty = false;
    middleNameDirty = false;
    genderDirty = false;
    ageDirty = false;
    dateOfBirthDirty = false;
    professionDirty = false;
    experienceDirty = false;
    deleteButtonTitle = "Delete";
    deleted = false;

    isNewCandidateAdding: boolean = false;

    professions = [
        {
            text: 'Select One',
            value: { id: null, title: "" }
        }
    ];

    genders = [
        {
            text: 'Select One',
            value: { id: null, title: "" }
        }
    ];


    @Watch('form.name')
    onNameChanged(value: string, oldValue: string) {
        this.nameDirty = true;
    }

    get nameState() {
        return this.nameDirty
            ? (this.form.name !== null
                && this.form.name !== ""
                && this.form.name.length <= 50
                && /^[A-Z]+$/.test(this.form.name[0]))
            : null;
    }

    @Watch('form.lastName')
    onLastNameChanged(value: string, oldValue: string) {
        this.lastNameDirty = true;
    }

    get lastNameState() {
        return this.lastNameDirty
            ? (this.form.lastName !== null
                && this.form.lastName !== ""
                && this.form.lastName.length <= 50
                && /^[A-Z]+$/.test(this.form.lastName[0]))
            : null;
    }

    @Watch('form.middleName')
    onMiddleNameChanged(value: string, oldValue: string) {
        this.middleNameDirty = true;
    }

    get middleNameState() {
        return this.middleNameDirty
            ? ((this.form.middleName.length <= 50
                && /^[A-Z]+$/.test(this.form.middleName[0])) || this.form.middleName.length === 0)
            : null;
    }

    @Watch('form.gender')
    onGenderChanged(value: string, oldValue: string) {
        this.genderDirty = true;
    }

    get genderState() {
        return this.genderDirty ? (this.form.gender.title !== null && this.form.gender.title !== "") : null;
    }

    @Watch('form.age')
    onAgeChanged(value: string, oldValue: string) {
        this.ageDirty = true;
    }

    get ageState() {
        return this.ageDirty ? (this.form.age !== null && this.form.age > 0) : null;
    }

    @Watch('form.dateOfBirth')
    onDateOfBirthChanged(value: string, oldValue: string) {
        this.dateOfBirthDirty = true;
    }

    get birthDateState() {
        return (this.form.dateOfBirth !== null);
    }

    @Watch('form.profession')
    onProfessionChanged(value: string, oldValue: string) {
        this.professionDirty = true;
    }

    get professionState() {
        return this.professionDirty ? (this.form.profession.title !== null && this.form.profession.title !== "") : null;
    }

    @Watch('form.yearsOfExperience')
    oneYearsOfExperienceChanged(value: string, oldValue: string) {
        this.experienceDirty = true;
    }

    get yearsOfExperienceState() {
        return this.experienceDirty ? (this.form.yearsOfExperience !== null && /^[0-9]+$/.test(String(this.form.yearsOfExperience))) : null;
    }

    isLetter(e: any) {
        let char = String.fromCharCode(e.keyCode);
        if (/^[A-Za-z]+$/.test(char)) return true;
        else e.preventDefault();
    }

    onSubmit(evt: any) {
        evt.preventDefault();

        var data = new CandidateModel();
        data.id = this.form.id;
        data.name = this.form.name;
        data.lastName = this.form.lastName;
        data.middleName = this.form.middleName;
        data.gender = this.form.gender;
        data.age = this.form.age;
        data.dateOfBirth = this.form.dateOfBirth;
        data.profession = this.form.profession;
        data.yearsOfExperience = this.form.yearsOfExperience;

        if (this.isNewCandidateAdding) {
            DataService.addItem(`${this.url}/addItem`, data)
                .then((response) => {
                    this.form.id = response.data.id;
                    this.isNewCandidateAdding = false;
                    this.$router.push({ name: "Search" });
                    console.log(response.data);
                })
                .catch((e) => {
                    console.log(e);
                });
        } else {
            DataService.updateItem(`${this.url}/update`, this.form.id, data)
                .then((response) => {
                    console.log(response.data);
                    this.$router.push({ name: "Search" });
                })
                .catch((e) => {
                    console.log(e);
                });
        }
    }

    onReset(evt: any) {
        evt.preventDefault();
        this.form = new CandidateModel();
    }

    getCandidate(id: string) {
        DataService.getItemById(`${this.url}/getById`, id)
            .then((response) => {

                var data = new CandidateModel();
                data.id = response.data.id;
                data.name = response.data.name;
                data.lastName = response.data.lastName;
                data.middleName = response.data.middleName;
                data.gender = response.data.gender;
                data.age = response.data.age;
                data.dateOfBirth = response.data.dateOfBirth;
                data.profession = response.data.profession;
                data.yearsOfExperience = response.data.yearsOfExperience;

                this.form = data;

                console.log(response.data);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    updateCandidate() {
        DataService.updateItem(`${this.url}/update`, this.form.id, this.form)
            .then((response) => {
                console.log(response.data);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    deleteCandidate() {
        DataService.deleteItem(`${this.url}/delete`, this.form.id)
            .then((response) => {
                this.deleteButtonTitle = "Deleted";
                this.deleted = true;
                console.log(response.data);
                this.$router.push({ name: "Search" });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getProfession() {
        DataService.getItems(`professions/getAll`)
            .then((response) => {
                for (var data of response.data) {
                    var prof = { id: data.id, title: data.title }
                    this.professions.push({ text: data.title, value: prof }) ;
                }
                console.log(response.data);
            })
            .catch((e) => {
                console.log(e);
            });

    }

    getGenders() {
        DataService.getItems(`genders/getAll`)
            .then((response) => {
                for (var data of response.data) {
                    var gender = { id: data.id, title: data.title }
                    this.genders.push({ text: data.title, value: gender }) ;
                }
                console.log(response.data);
            })
            .catch((e) => {
                console.log(e);
            });

    }


    mounted() {
        this.form.gender = new Gender();
        this.form.profession = new Profession();
        this.getProfession();
        this.getGenders();

        var id = this.$route.params.id;

        if (id != null && id !== "" && id !== "new") {
            this.getCandidate(id);
            this.isNewCandidateAdding = false;
        }
        else {
            this.isNewCandidateAdding = true;
        }
    }
}
