﻿import { Gender } from "./Gender";
import { Profession } from "./Profession";

export class CandidateModel {
    public id: any = null;    
    public name: string = "";
    public lastName: string = "";
    public middleName: string = "";
    public gender: Gender = new Gender();
    public age: number | null = null;
    public dateOfBirth: Date | null = null;
    public profession: Profession = new Profession(); 
    public yearsOfExperience: number | null = null;
}

export default new CandidateModel();