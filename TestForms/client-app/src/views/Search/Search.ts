import { Component, Vue } from 'vue-property-decorator';
import DataService from "../../services/DataService";

@Component({ components: {} })
export default class CandidatesList extends Vue {
    candidates: any = [];
    currentCandidate: any = null;
    currentIndex: number = -1;
    value: string = "";
    url = "candidates";

    retrieveCandidates() {
        DataService.getItems(`${this.url}/getAll`)
            .then((response) => {
                this.candidates = response.data;
                this.candidates.gender = response.data.gender;
                this.candidates.profession = response.data.profession;
                console.log(response.data);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    setActiveCandidate(candidate: any, index: number) {
        this.currentCandidate = candidate;
        this.currentIndex = index;
    }

    searchValue() {
        DataService.getItemsByValue(`${this.url}/getByValue`, this.value)
            .then((response) => {
                this.candidates = response.data;
                console.log(response.data);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    addCandidate() {
        this.$router.push({ name: "CandidateNew" });
    }

    mounted() {
        this.retrieveCandidates();
    }
}