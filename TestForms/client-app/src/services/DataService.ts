import http from "../http-common";

class DataService {
    getItems(url: string) {
        return http.get(url);
    }

    getItemsByCriteria(url: string, criteria: any) {
        return http.get(`${url}`, criteria);
    }

    getItemById(url: string, id: string) {
        return http.get(`${url}?id=${id}`);
    }

    getItemsByValue(url: string, value: string) {
        return http.get(`${url}?value=${value}`);
    }

    addItem(url: string, data: any) {
        return http.post(`${url}`, data);
    }

    updateItem(url: string, id: string, data: any) {
        return http.put(`${url}?id=${id}`, data);
    }

    deleteItem(url: string, id: string) {
        return http.delete(`${url}?id=${id}`);
    }
}

export default new DataService();
