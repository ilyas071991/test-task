﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TestForms.BusinessLogic;
using TestForms.DataAccess.Contexts;
using TestForms.Entity;

namespace TestForms.Controllers.WebApi
{
    [Route("api/professions")]
    public class ProfessionController : Controller
    {
        private readonly ProfessionBusinessLogic _professionBusinessLogic;

        public ProfessionController(TestFormsDbContext context)
        {
            _professionBusinessLogic = new ProfessionBusinessLogic(context);
        }

        [HttpGet]
        [Route("getAll")]
        public IEnumerable<Profession> GetAll()
        {
            var professions = _professionBusinessLogic.GetAll();
            return professions;
        }

    }
}