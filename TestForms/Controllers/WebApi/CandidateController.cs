﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestForms.BusinessLogic;
using TestForms.DataAccess.Contexts;
using TestForms.Entity;

namespace TestForms.Controllers.WebApi
{
    [Route("api/candidates")]
    public class CandidateController : Controller
    {
        private readonly CandidateBusinessLogic _candidateBusinessLogic;

        public CandidateController(TestFormsDbContext context)
        {
            _candidateBusinessLogic = new CandidateBusinessLogic(context);
        }

        [HttpGet]
        [Route("getAll")]
        public IEnumerable<Candidate> GetAll()
        {
            var candidates = _candidateBusinessLogic.GetAll();
            return candidates;
        }

        [HttpGet]
        [Route("getByCriteria")]
        public IEnumerable<Candidate> GetByCriteria([FromBody]CandidateSearchCriteria criteria)
        {
            return _candidateBusinessLogic.GetByCriteria(criteria);
        }

        [HttpGet("{value}")]
        [Route("getByValue")]
        public IEnumerable<Candidate> GetByValue(string value)
        {
            var result = _candidateBusinessLogic.GetByValue(value);
            return result;
        }

        [HttpGet("{id}")]
        [Route("getById")]
        public Candidate Get(int id)
        {
            return _candidateBusinessLogic.Get(id);
        }

        [HttpPost]
        [Route("addItem")]
        public async Task<IActionResult> Post([FromBody] Candidate candidate)
        {
            if (ModelState.IsValid)
            {
                await _candidateBusinessLogic.Create(candidate);
                return Ok(candidate);
            }
            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        [Route("update")]
        public async Task<IActionResult> Put(int id, [FromBody] Candidate candidate)
        {
            if (ModelState.IsValid)
            {
                await _candidateBusinessLogic.Update(candidate);
                return Ok(candidate);
            }
            return BadRequest(ModelState);
        }

        [HttpDelete("{Id}")]
        [Route("delete")]
        public IActionResult Delete(int id)
        {
            var candidate = _candidateBusinessLogic.Get(id);
            if (candidate != null)
            {
                _candidateBusinessLogic.Delete(candidate);
            }
            return Ok(candidate);
        }

    }
}