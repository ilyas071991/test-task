﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TestForms.BusinessLogic;
using TestForms.DataAccess.Contexts;
using TestForms.Entity;

namespace TestForms.Controllers.WebApi
{
    [Route("api/genders")]
    public class GenderController : Controller
    {
        private readonly GenderBusinessLogic _genderBusinessLogic;

        public GenderController(TestFormsDbContext context)
        {
            _genderBusinessLogic = new GenderBusinessLogic(context);
        }

        [HttpGet]
        [Route("getAll")]
        public IEnumerable<Gender> GetAll()
        {
            var genders = _genderBusinessLogic.GetAll();
            return genders;
        }

    }
}