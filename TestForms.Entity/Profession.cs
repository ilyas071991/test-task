﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace TestForms.Entity
{
    public class Profession
    {
        public int? Id { get; set; }
        public string Title { get; set; }

        [JsonIgnore]
        public List<Candidate> Candidates { get; set; } = new List<Candidate>();
    }
}