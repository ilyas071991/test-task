﻿using System;
using System.ComponentModel.DataAnnotations;
using TestForms.Entity.Interfaces;

namespace TestForms.Entity
{
    public class Candidate : ICandidate
    {
        public int? Id { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 1)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z]*$")]
        public string Name { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 1)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z]*$")]
        public string LastName { get; set; }
        [StringLength(50)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z]*$")]
        public string MiddleName { get; set; }
        public Gender Gender { get; set; }
        public int Age { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateOfBirth { get; set; }
        public Profession Profession { get; set; }
        public int YearsOfExperience { get; set; }
    }
}