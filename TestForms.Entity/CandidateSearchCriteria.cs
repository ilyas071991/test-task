﻿using TestForms.Entity.Interfaces;

namespace TestForms.Entity
{
    public class CandidateSearchCriteria: ICandidateSearchCriteria
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public Profession Profession { get; set; }
        public int? YearsOfExperience { get; set; }
    }
}