﻿namespace TestForms.Entity.Enums
{
    public enum ProfessionType
    {
        Manager,
        Analyst,
        Developer,
        Tester
    }
}