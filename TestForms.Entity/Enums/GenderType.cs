﻿namespace TestForms.Entity.Enums
{
    public enum GenderType
    {
        Male = 0,
        Female = 1
    }
}