﻿namespace TestForms.Entity.Interfaces
{
    public interface ICandidateSearchCriteria
    {
        int Id { get; set; }
        string Name { get; set; }
        string LastName { get; set; }
        Profession Profession { get; set; }
        int? YearsOfExperience { get; set; }
    }
}