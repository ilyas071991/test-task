﻿using System;

namespace TestForms.Entity.Interfaces
{
    public interface ICandidate
    {
        int? Id { get; set; }
        string Name { get; set; }
        string LastName { get; set; }
        string MiddleName { get; set; }
        Gender Gender { get; set; }
        int Age { get; set; }
        DateTime DateOfBirth { get; set; }
        Profession Profession { get; set; }
        int YearsOfExperience { get; set; }
    }
}